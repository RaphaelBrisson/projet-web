import { StyleSheet } from 'react-native';

export default StyleSheet.create({

    // Global style

    container: {
        paddingTop: 30,
        paddingHorizontal: 25,
    },

    container__fullWidth: {
        paddingTop: 30,
    },

    paddingHorizontal: {
        paddingHorizontal: 25,
    },

    center: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center",
    },

    flexRow: {
        flexDirection: 'row',
    },

    alignItemsCenter: {
        alignItems: 'center',
        textAlign: 'center',
    },

    spaceBetween: {
        justifyContent: 'space-between',
        width: '100%',
    },

    textStyle: {
        color: "#2C4168",
    },

    titleStyle: {
        color: "#92a8d1",
        fontFamily: 'Liquido',
    },

    btnStyle: {
        paddingVertical: 12,
        paddingHorizontal: 25,
        backgroundColor: "#92a8d1",
        borderRadius: 50,
        backgroundColor: "#f9faff",
        shadowColor: "#2C4168",
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.5,
        shadowRadius: 15,
        elevation: 8,
    },

    btnStyleGreen: {
        backgroundColor: '#3CB371',
        color: '#ffffff',
    },

    linkStyle: {
        paddingVertical: 12,
        color: "#2C4168",
        textDecorationLine: 'underline',
    },

    btnTextStyle: {
        color: "#2C4168",
        textAlign: 'center',
        fontSize: 18,
        fontWeight: '700'
    },

    btnTextStyleGreen: {
        color: "#ffffff",
    },

    mb0: {
        marginBottom: 0,
    },

    mb10: {
        marginBottom: 10,
    },

    mb20: {
        marginBottom: 20,
    },
    
    fwBold: {
        fontWeight: '700',
    },

    introTitle: {
        fontSize: 22,
        fontWeight: '700',
        color: '#2C4168',
        marginTop: 30,
        width: '80%',
    },

    introText: {
        fontSize: 17,
        color: '#2C4168',
        marginBottom: 30,
        width: '80%',
    },

    activityIndicator: {
        marginTop: 300,
    },

    
    // Profil
    
    profilPicture: {
        width: 150,
        height: 150,
        borderRadius: 100,
        backgroundColor: '#EBEEFE',
        shadowColor: "#2C4168",
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.5,
        shadowRadius: 15,
        elevation: 8,
    },

    pseudo: {
        fontSize: 18,
        fontWeight: '700',
        marginTop: 15,
    },

    lvlStyle: {
        fontSize: 27,
        fontWeight: '700',
    },

    // Défis

    defiCard: {
        marginVertical: 10,
        paddingHorizontal: 20,
        paddingVertical: 15,
        backgroundColor: '#ffffff',
        borderRadius: 26,
        shadowColor: "#2C4168",
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.05,
        shadowRadius: 15,
        elevation: 8,
    },
    
    defiTitle: {
        fontSize: 28,
    },

    defiIcone: {
        width: 30,
        height: 30,
        borderRadius: 15,
    },

    defiDescription: {
        paddingTop: 10,
        paddingBottom: 15,
    },

    defiDifficulteEtoiles: {
        width: 75,
        height: 18,
    },

    defiDifficulteXP: {
        fontWeight: '700',
        textAlign: 'center'
    },


    // Publications

    publiCategoryItem: {
        alignItems: 'center',
        marginBottom: 10,
    },

    publiCategoryLabel: {
        marginTop: -10,
        fontSize: 18,
        fontWeight: '700',
    },

    profilPicture__small: {
        width: 40,
        height: 40,
        marginRight: 10,
    },

    publiSingleItem: {
        marginVertical: 10,
        paddingHorizontal: 20,
        paddingVertical: 20,
        backgroundColor: '#ffffff',
        borderRadius: 26,
        shadowColor: "#2C4168",
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.05,
        shadowRadius: 15,
        elevation: 8,
    },

    publiSingleItemCommentaire: {
        paddingBottom: 10,
        paddingTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#DDD',
    },

    publiSingleImg: {
        height: 200,
        marginBottom: 10,
    },
    
    publiSingleVid: {
        height: 200,
        width: '100%',
        marginHorizontal: 0,
        paddingHorizontal: 0,
        marginBottom: 10,
    },

    publiSingleTitle: {
        fontSize: 28,
        maxWidth: '90%',
        paddingTop: 5,
        paddingBottom: 10,
    }, 

    publiSingleVote: {
        marginRight: 3
    },

    publiSingleNbcommentaires: {
        textDecorationLine: 'underline',
        paddingTop: 10,
    },

    detailPubliSingleDescription: {
        paddingBottom: 20,
        marginBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#DDD',
    },

    sortBtns: {
        paddingHorizontal: 30,
        paddingBottom: 20,
    },

    sortBtn: {
        color: "#2C4168",
        borderRadius: 26,
        backgroundColor: '#FFFFFF',
        paddingHorizontal: 25,
        paddingVertical: 6,
        fontSize: 15,
    },

    ajouterBtn: {
        position: 'absolute',
        backgroundColor: '#92a8d1',
        paddingVertical: 5,
        paddingHorizontal: 15,
        borderRadius: 100,
        bottom: 15,
        right: 25,
    },

    ajouterBtnTxt: {
        fontSize: 24,
        color: 'white'
    },

    envoyerCommentaire: {
        position: 'absolute',
        right: 20,
        top: 12,
    },

    baleineBackground: {
        position: 'absolute',
        right: 0,
        width: 90,
        height: 87,
    },

    baleineHeader: {
        marginBottom: 30,
        width: 49,
        height: 31,
    },
});
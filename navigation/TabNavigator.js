import React from "react";
import { Ionicons } from '@expo/vector-icons'; 
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { ProfilStackNavigator, DefisStackNavigator, PublisStackNavigator } from "./StackNavigator";

const Tab = createBottomTabNavigator();
const BottomTabNavigator = () => {
  return (
    <Tab.Navigator
    screenOptions={({ route }) => ({
      tabBarIcon: ({ focused, color, size }) => {
        if (route.name === 'Profil') {
          return (
            <Ionicons
              name={focused ? 'person-circle' : 'person-circle-outline'}
              size={size}
              color={color}
            />
          );
        } else if (route.name === 'Defis') {
          return (
            <Ionicons
              name={focused ? 'trophy' : 'trophy-outline'}
              size={size}
              color={color}
            />
          );
        } else if (route.name === 'Conseils') {
          return (
            <Ionicons
              name={focused ? 'newspaper' : 'newspaper-outline'}
              size={size}
              color={color}
            />
          );
        }
      },
    })}
    tabBarOptions={{
      activeTintColor: '#92a8d1',
      inactiveTintColor: '#AAAAAA',
    }}
    
    >
      <Tab.Screen name="Conseils" component={PublisStackNavigator} />
      <Tab.Screen name="Defis" component={DefisStackNavigator} options={{ title: 'Défis' }} />
      <Tab.Screen name="Profil" component={ProfilStackNavigator} />
    </Tab.Navigator>
  );
};
export default BottomTabNavigator;
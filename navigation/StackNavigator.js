import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import Profil from "../screens/Profil/Profil";
import ModifProfil from "../screens/Profil/ModifProfil";

import Defis from "../screens/Defis/Defis";
import DetailDefi from "../screens/Defis/DetailDefi";

import Publications from "../screens/Publications/Publications";
import ListePublications from "../screens/Publications/ListePublications";
import DetailPublication from "../screens/Publications/DetailPublication";
import AjouterPublication from "../screens/Publications/Creation/AjouterPublication";

const Stack = createStackNavigator();
const screenOptionStyle = {
  headerStyle: {
    backgroundColor: "#92A8D1",
  },
  headerTintColor: "white",
  headerBackTitle: "Back",
  cardStyle: { backgroundColor: '#F9FAFF' },
  headerTitleStyle: {
    fontFamily: "Liquido",
    fontSize: 28,
  },
};

const ProfilStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Profil" component={Profil} options={{ title: 'Mon profil' }} />
      <Stack.Screen name="ModifProfil" component={ModifProfil} options={{ title: 'Modifier mon profil' }} />
    </Stack.Navigator>
  );
};

const DefisStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Defis" component={Defis} options={{ title: 'Mes défis' }} />
      <Stack.Screen name="DetailDefi" component={DetailDefi} options={{ title: 'Mes défis' }} />
    </Stack.Navigator>
  );
};

const PublisStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Publications" component={Publications} options={{ title: 'Mon fil d\'actualités' }} />
      <Stack.Screen name="ListePublications" component={ListePublications} options={{ title: 'Mon fil d\'actualités' }} />
      <Stack.Screen name="DetailPublication" component={DetailPublication} options={{ title: 'Mon fil d\'actualités' }} />
      <Stack.Screen name="AjouterPublication" component={AjouterPublication} options={{ title: 'Ajouter une publication' }} />
    </Stack.Navigator>
  );
};

export { ProfilStackNavigator, DefisStackNavigator, PublisStackNavigator };
# On ne sait pas le nom de l'appli mais nsm

## API

### Publications

Récupérer toutes les publications. *tout sauf commentaires*
```php
GET /publications
```

Récupérer une publication grâce à son ID. *tout*
```php
GET /publication/{id}
```

Créer une publication
```php
POST /publication
```

Mettre à jour une publication
```php
PUT /publication/{id}
```

Supprimer une publication
```php
DELETE /publication/{id}
```

#### Commentaires

Ajouter un commentaire à la publication.
```php
POST /publication/{id}/commentaire
```

Modifier un commentaire de la publication.
```php
PUT /publication/{id}/commentaire
```

Supprimer un commentaire de la publication.
```php
DELETE /publication/{id}/commentaire
```

#### Votes

Ajouter un vote à la publication.
```php
POST /publication/{id}/vote
```

Suppriler un vote de la publication.
```php
DELETE /publication/{id}/vote
```

### Défis

Récupérer tous les défis.
```php
GET /defis
```

Récupérer un défi grâce à son ID.
```php
GET /defi/{id}
```

Créer un défi
```php
POST /defi
```

Mettre à jour un défi
```php
PUT /defi/{id}
```

Supprimer un défi
```php
DELETE /defi/{id}
```

### Utilisateurs

Récupérer tous les utilisateurs.
```php
GET /utilisateurs
```

Récupérer un utilisateur grâce à son ID.
```php
GET /utilisateur/{id}
```

Créer un utilisateur.
```php
POST /utilisateur
```

Mettre à jour un utilisateur.
```php
PUT /utilisateur/{id}
```

Supprimer un utilisateur.
```php
DELETE /utilisateur/{id}
```

#### Niveaux

Mise à jour du niveau de l'utilisateur.
```php
PUT /utilisateur/{id}/niveau
```

### Niveaux

Ajouter un niveau.
```php
POST /niveau
```

Modifier un niveau.
```php
PUT /niveau
```




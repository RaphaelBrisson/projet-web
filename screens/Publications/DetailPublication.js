import React, { useEffect, useState } from "react";
import { Ionicons } from '@expo/vector-icons'; 
import { ActivityIndicator, View, Text, FlatList, Image, TouchableOpacity } from "react-native";
import Moment from 'moment';
import 'moment/locale/fr';
import { TextInput } from 'react-native-paper';
import styles from '../../style/style.js';

function dateFormat(date) {
  Moment.locale('fr');
  return(Moment(date).format('L à HH:mm'));
}

const Item = ({ item }) => (
  <View style={styles.publiSingleItemCommentaire}>
    <View style={[styles.paddingHorizontal, styles.flexRow, styles.spaceBetween, {paddingBottom: 5}]}>
      <Text style={[styles.textStyle, styles.fwBold]}>{item.author.username}</Text>
      <Text style={styles.textStyle}>Le {dateFormat(item.publishedAt)}</Text>
    </View>
    <Text style={[styles.paddingHorizontal, styles.textStyle]}>{item.content}</Text>
  </View>
);

const DetailPublication = ({ route, navigation }) => {
  const [commentaire, setCommentaire] = React.useState('');
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const params = route.params;
  let request = 'http://whaledone.iut-lepuy.fr/api/posts/' + params.id;

  useEffect(() => {
    fetch(request)
    .then((response) => response.json())
    .then((json) => setData(json))
    .catch((error) => console.error(error))
    .finally(() => setLoading(false));
  }, []);

  //console.log(data);

  function dateFormat(date) {
    Moment.locale('fr');
    return(Moment(date).format('L à HH:mm'));
  }

  // async function votePublication() {
  //   data.likes = data.likes + 1;
  // }

  const renderPublication = () => {
    return (
      <View style={styles.container__fullWidth}>
        <View style={[styles.flexRow, styles.paddingHorizontal, styles.mb10]}>
          {data.author.avatar ? 
            <Image style={[styles.profilPicture, styles.profilPicture__small]} source={{uri: data.author.avatar}}/> 
            : <View style={[styles.profilPicture, styles.profilPicture__small]}></View>
          }
          <View>
            <Text style={styles.textStyle}>{data.author.username}</Text>
            <Text style={styles.textStyle}>Publié le {dateFormat(data.publishedAt)}</Text>
          </View>
        </View>
        <View style={[styles.flexRow, styles.spaceBetween, styles.paddingHorizontal, styles.alignItemsCenter]}>
          <Text style={[styles.publiSingleTitle, styles.titleStyle]}>{data.title}</Text>
          <TouchableOpacity>
            <View style={[styles.flexRow, styles.alignItemsCenter]}>
              {data.likes == 0 ? 
                <Text style={[styles.publiSingleVote, styles.textStyle]}>0</Text>
                : <Text style={[styles.publiSingleVote, styles.textStyle]}>{data.likes}</Text>
              }
              <Ionicons name={'chevron-up-circle-outline'} size={24} color="#68778e" />
            </View>
          </TouchableOpacity>
        </View>
        {data.image ? <Image style={styles.publiSingleImg} source={{uri: data.image}}/> : null }
        {data.video ? 
          <View style={{position: 'relative'}}>
            <Video
              ref={React.useRef(null)}
              style={styles.publiSingleVid}
              source={{
                uri: data.video,
              }}
              useNativeControls
              resizeMode="contain"
              isLooping
            />
            <Ionicons name={'play'} size={24} color="white" style={{position: 'absolute', left: 10, top: 10,}} />
          </View>
        : null }
        <Text style={[styles.paddingHorizontal, styles.detailPubliSingleDescription, styles.textStyle]}>{data.content}</Text>
        {/* <Text style={[styles.paddingHorizontal, styles.publiSingleNbcommentaires, styles.textStyle]}>{data.nbcommentaires} commentaires</Text> */}
      </View>
     );
  }

  const renderItem = ({ item }) => {
      return (
          <Item
          item={item}
          />
      );
  };

  return (
    <View>
      {isLoading ? <ActivityIndicator size="large" color="#92A8D1" style={styles.activityIndicator}/> :
        <View style={{paddingBottom: 63}}>
          <FlatList
            data={data.comments}
            renderItem={renderItem}
            ListHeaderComponent={renderPublication}
          />
          <View style={{position: 'absolute', bottom: 0, width: '100%'}}>
            <TextInput
              label="Ajouter un commentaire..."
              value={commentaire}
              onChangeText={commentaire => setCommentaire(commentaire)}
              style={{width: '100%', backgroundColor: "#EBEEFE"}}
              theme={{colors: {primary: "#92a8d1", text: "#2C4168", placeholder: "#68778e" }, roundness: 23}}
            />
            <TouchableOpacity style={styles.envoyerCommentaire}>
              <Text style={[styles.linkStyle]}>Envoyer</Text>
            </TouchableOpacity>
          </View>
        </View>
      }
    </View>
  );
};

export default DetailPublication;
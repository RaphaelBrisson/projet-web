import React, { useState } from "react";
import { View, Text, FlatList, Image, TouchableOpacity } from "react-native";
import styles from '../../style/style.js';

const DATA = [
  {
    id: '1',
    title: 'Jardinage',
    image: require('../../assets/img/jardinage.png'),
  },
  {
    id: '2',
    title: 'Déchets',
    image: require('../../assets/img/dechets.png'),
  },
  {
    id: '3',
    title: 'Habitation',
    image: require('../../assets/img/immeuble.png'),
  },
  {
    id: '4',
    title: 'Lorem ipsum',
    image: require('../../assets/img/jardinage.png'),
  },
  {
    id: '5',
    title: 'Lorem ipsum',
    image: require('../../assets/img/dechets.png'),
  },
  {
    id: '6',
    title: 'Lorem ipsum',
    image: require('../../assets/img/immeuble.png'),
  },
  {
    id: '7',
    title: 'Lorem ipsum',
    image: require('../../assets/img/immeuble.png'),
  },
];

const Item = ({ item, onPress }) => (
  <TouchableOpacity onPress={onPress} style={styles.publiCategoryItem}>
    <Image source={item.image} style={{width: 114, height: 112}} />
    <Text style={[styles.publiCategoryLabel, styles.textStyle]}>{item.title}</Text>
  </TouchableOpacity>
);

const Publications = ({ navigation }) => {

  const renderItem = ({ item }) => {

    return (
      <Item
        item={item}
        onPress={() => navigation.navigate("ListePublications")}
      />
    );
  };

  return (
    <View style={[styles.center, styles.container]}>
      <Text style={[styles.textStyle, {fontWeight: '700', fontSize: 27, marginBottom: 30}]}>Choisissez un domaine</Text>
      <FlatList
        data={DATA}
        renderItem={renderItem}
        numColumns={2}
        columnWrapperStyle={{justifyContent: 'space-between'}}
        style={{width: '85%'}}
      />
    </View>
  );
};

export default Publications;

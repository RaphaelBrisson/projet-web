import React, { useState, useEffect } from 'react';
import { TextInput } from 'react-native-paper';
import { View, Text, TouchableOpacity, ScrollView, Image, Platform } from "react-native";
import * as ImagePicker from 'expo-image-picker';
import styles from '../../../style/style.js';

const AjouterPublication = () => {
  const [title, setTitle] = React.useState('');
  const [content, setContent] = React.useState('');

  const [response, setResponse] = React.useState('');
  const [image, setImage] = useState(null);

  useEffect(() => {
    (async () => {
      if (Platform.OS !== 'web') {
        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
          alert('Nous avons besoin de votre autorisation pour que ça marche !');
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    console.log(result);

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

  function postPost() {

    useEffect(() => {
      fetch('https://mywebsite.com/endpoint/', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          sponsored: false,
          summary: "test Eros diam egestas libero eu vulputate risus. Morbi tempus commodo mattis. Pellentesque vitae velit ex. Lorem ipsum dolor sit amet consectetur adipiscing elit. In hac habitasse platea dictumst. Mauris dapibus risus quis suscipit vulputate.",
          tags: ['Jardinage'],
          title: "test Ut suscipit posuere justo at vulputate",
          image: null,
        })
      });
    }, []);
  }

  return (
    <ScrollView>
      <View style={[styles.container, styles.center, {marginBottom: 20}]}>
      <View style={[{width: '100%'}]}>
        <TextInput
          label="Titre de mon conseil"
          value={title}
          onChangeText={title => setTitle(title)}
          style={{width: '100%', backgroundColor: "#EBEEFE", marginBottom: 20, borderRadius: 23}}
          theme={{colors: {primary: "#92a8d1", text: "#2C4168", placeholder: "#68778e"}, roundness: 23 }}
          underlineColor="transparent"
        />
        <TextInput
          label="Le contenu"
          multiline={true}
          numberOfLines={8}
          value={content}
          onChangeText={content => setContent(content)}
          style={{width: '100%', backgroundColor: "#EBEEFE", marginBottom: 10, borderRadius: 23 }}
          theme={{colors: {primary: "#92a8d1", text: "#2C4168", placeholder: "#68778e" }, roundness: 23 }}
          underlineColor="transparent"
        />
      </View>
      <View style={[styles.alignItemsCenter]}>
        <TouchableOpacity onPress={pickImage} style={{flexDirection: 'row', alignItems: 'center', marginTop: 25 }}>
          <Image source={require('../../../assets/img/add_image.png')} style={{width: 42, height: 36, marginRight: 10 }} />
          <Text style={styles.linkStyle}>Ajouter une image</Text>
        </TouchableOpacity>
        {image && <Image source={{ uri: image }} style={{ width: 320, height: 200, marginBottom: 20 }} />}

        <TouchableOpacity style={[styles.btnStyle, {marginTop: 30}]} onPress={postPost}>
          <Text style={styles.btnTextStyle}>Je publie</Text>
        </TouchableOpacity>
      </View>
    </View>
    </ScrollView>
  );
};

export default AjouterPublication;
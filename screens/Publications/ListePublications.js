import React, { useState, useEffect } from "react";
import { Ionicons } from '@expo/vector-icons'; 
import { View, Text, FlatList, Image, TouchableOpacity, ActivityIndicator } from "react-native";
import Moment from 'moment';
import 'moment/locale/fr';
import { Video } from 'expo-av';
import styles from '../../style/style.js';

function dateFormat(date) {
  Moment.locale('fr');
  return(Moment(date).format('L à HH:mm'));
}

const Item = ({ item, onPress }) => (
  <View style={styles.paddingHorizontal}>
    <TouchableOpacity onPress={onPress} style={styles.publiSingleItem}>
      <View style={[styles.flexRow, styles.mb10]}>
        {item.author.avatar ? 
          <Image style={[styles.profilPicture, styles.profilPicture__small]} source={{uri: item.author.avatar}}/> 
          : <View style={[styles.profilPicture, styles.profilPicture__small]}></View>
        }
        <View>
          <Text style={[styles.textStyle, styles.fwBold, {fontSize: 16}]}>{item.author.username}</Text>
          <Text style={[styles.textStyle, {fontSize: 12}]}>Publié le {dateFormat(item.publishedAt)}</Text>
        </View>
      </View>
      <View style={[styles.flexRow, styles.spaceBetween, styles.alignItemsCenter]}>
        <Text style={[styles.publiSingleTitle, styles.titleStyle]}>{item.title}</Text>
        <View style={[styles.flexRow, styles.alignItemsCenter]}>
          {item.likes == 0 ? 
            <Text style={[styles.publiSingleVote, styles.textStyle]}>0</Text>
            : <Text style={[styles.publiSingleVote, styles.textStyle]}>{item.likes}</Text>
          }
          <Ionicons name={'chevron-up-circle-outline'} size={24} color="#68778e" />
        </View>
      </View>
      {item.image ? <Image style={styles.publiSingleImg} source={{uri: item.image}}/> : null }
      {item.video ? 
        <View style={{position: 'relative'}}>
          <Video
            ref={React.useRef(null)}
            style={styles.publiSingleVid}
            source={{
              uri: item.video,
            }}
            useNativeControls
            resizeMode="contain"
            isLooping
          />
          <Ionicons name={'play'} size={24} color="white" style={{position: 'absolute', left: 10, top: 10,}} />
        </View>
      : null }
      
      <Text style={[styles.textStyle, styles.mb10]}>{item.summary}</Text>
      {/* <Text style={[styles.paddingHorizontal, styles.publiSingleNbcommentaires, styles.textStyle]}>{item.comments} commentaires</Text> */}
    </TouchableOpacity>
  </View>
    
);


const ListePublications = ({ navigation }) => {
  const [isLoading, setLoading] = useState(true);
  let [data, setData] = useState([]);

  useEffect(() => {
    fetch('http://whaledone.iut-lepuy.fr/api/posts', {method: 'GET',
      headers: {
          'Content-Type'    : 'application/json',
          'Accept'          : 'application/json',
      }})
      .then((response) => response.json())
      .then((json) => setData(json))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  //console.log(data);
  
  data = data.sort( (a, b) => b.likes - a.likes );

  function sortByDate() {
    data.sort( (a, b) => a.publishedAt.localeCompare(b.publishedAt) );
    setData([...data]);
  }
  function sortByVotes() {
    data.sort( (a, b) => b.likes - a.likes );
    setData([...data]);
  }

  const renderItem = ({ item }) => {
    return (
      <Item
        item={item}
        onPress={() => navigation.navigate("DetailPublication", {
          id: item.id,             
        })}
      />
    );
  };

  const renderHeader = () => {
    return (
      <View style={styles.paddingHorizontal}>
        <View style={styles.center}>
          <Image
            style={styles.baleineHeader}
            source={require('../../assets/img/logo_baleine_header.png')}
          />
        </View>
        <View style={[styles.flexRow, styles.sortBtns, styles.spaceBetween]}>
          <TouchableOpacity onPress={sortByDate}>
            <Text style={styles.sortBtn}>Récent</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={sortByVotes}>
            <Text style={styles.sortBtn}>Populaire</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.introTitle}>Bonjour,</Text>
        <Text style={styles.introText}>Voici les nouvelles du jour !</Text>
        <Image
          style={[styles.baleineBackground, {top: 120}]}
          source={require('../../assets/img/baleine_background.png')}
        />
      </View>
    );
  }

  return (
      <View>
        {isLoading ? <ActivityIndicator size="large" color="#92A8D1" style={styles.activityIndicator}/> :
          <View>
            <FlatList
              style={styles.container__fullWidth}
              data={data}
              renderItem={renderItem}
              contentContainerStyle={{paddingBottom: 30}}
              ListHeaderComponent={renderHeader}
            />
            <TouchableOpacity onPress={() => navigation.navigate("AjouterPublication")} style={styles.ajouterBtn}>
              <Text style={styles.ajouterBtnTxt}>+</Text>
            </TouchableOpacity>
          </View>
        }
        
      </View>
  );
};

export default ListePublications;


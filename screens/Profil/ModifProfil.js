import React from "react";
import { View, StyleSheet, Text, Button } from "react-native";
const ModifProfil = ({ navigation }) => {
  return (
    <View style={styles.center}>
        <Text>Page pour modifier le profil</Text>
    </View>
  );
};
const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
  },
});
export default ModifProfil;
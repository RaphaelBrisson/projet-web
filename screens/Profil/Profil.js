import React, { useEffect, useState } from "react";
import { ActivityIndicator, View, Text, Image, TouchableOpacity } from "react-native";
import * as Progress from 'react-native-progress';
import styles from '../../style/style.js';


const Profil = ({ navigation }) => {

  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [dataLvl, setDataLvl] = useState([]);

  useEffect(() => {
    fetch('http://whaledone.iut-lepuy.fr/api/users/6')
      .then((response) => response.json())
      .then((json) => setData(json))
      .catch((error) => console.error(error))
      .then(()=>{
        fetch('http://whaledone.iut-lepuy.fr/api/levels/' + data.level)
        .then((response) => response.json())
        .then((json) => setDataLvl(json))
        .catch((error) => console.error(error))
        .finally(() => setLoading(false))
      });
  }, []);

  //console.log(data);

  let progressBarPercentage = ((data.xp - dataLvl.min)/(dataLvl.max - dataLvl.min));
  let leftForNextLvl = (dataLvl.max - data.xp);

  console.log(progressBarPercentage);
  console.log(leftForNextLvl);

  return (
    <View>
      {isLoading ? <ActivityIndicator size="large" color="#92A8D1" style={styles.activityIndicator} /> : 
      <View>
        <Image style={[styles.baleineBackground, {top: 410}]} source={require('../../assets/img/baleine_background.png')} />
        <View style={[styles.alignItemsCenter, styles.container]}>
                <View style={styles.alignItemsCenter}>
            <Image
              style={[styles.baleineHeader]}
              source={require('../../assets/img/logo_baleine_header.png')}
            />
          </View>
          <View style={styles.alignItemsCenter}>
              {data.avatar ? 
                <Image style={styles.profilPicture} source={{uri: data.avatar}} /> 
                : <View style={styles.profilPicture}></View>
              }
              <Text style={[styles.pseudo, styles.textStyle, {marginBottom: 5}]}>{data.username}</Text>
          </View>
          <View style={styles.alignItemsCenter}>
              <Text style={[styles.textStyle, {marginBottom: 5}]}>{data.email}</Text>
              <Text style={[styles.textStyle]}>Lyon</Text>
              <TouchableOpacity onPress={() => navigation.navigate("ModifProfil")}>
                <Text style={styles.linkStyle}>Changer mes informations</Text>
              </TouchableOpacity>
          </View>

          <View style={[styles.alignItemsCenter, {marginTop: 20, marginLeft: -60, width: '50%'}]}>
              <Text style={[styles.textStyle, styles.lvlStyle]}>Niveau {data.level}</Text>
              <Text style={[styles.textStyle, styles.mb10]}><Text style={[styles.fwBold]}>{data.xp}</Text> points cumulés</Text>
              <Progress.Bar progress={progressBarPercentage} width={160} height={14} color={"#2C4168"} unfilledColor={"#EEF1FE"} borderWidth={0} />
              <Text style={[styles.textStyle, styles.mb10, {textAlign: 'center', marginTop: 10}]}>Plus que <Text style={[styles.fwBold]}>{leftForNextLvl} points</Text> pour atteindre le niveau 2 !</Text>
          </View>
        </View>
      </View>
      }
    </View>
  );
};

export default Profil;
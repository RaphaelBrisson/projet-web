import React, { useState, useEffect } from "react";
import { View, Text, FlatList, Image, TouchableOpacity, ActivityIndicator } from "react-native";
import styles from '../../style/style.js';

const Item = ({ item, onPress }) => (
  <View style={styles.paddingHorizontal}>
    <TouchableOpacity onPress={onPress} style={styles.defiCard}>
      <View style={[styles.flexRow, styles.spaceBetween, styles.alignItemsCenter]}>
        <Text style={[styles.defiTitle, styles.titleStyle]}>{item.title}</Text>
        {/* <Image
          style={styles.defiIcone}
          source={require('../../assets/img/pdp.jpg')}
        /> */}
      </View>
      <Text style={[styles.defiDescription, styles.textStyle]}>{item.summary}</Text>
      <View style={[styles.flexRow, styles.spaceBetween]}>
        <View>
          <Text style={[styles.defiDifficulteXP, styles.textStyle]}>Difficulté</Text>
          {item.difficulty == 'Facile' ? <Image style={styles.defiDifficulteEtoiles} source={require('../../assets/img/difficulte_facile.png')}/>
          : item.difficulty == 'Moyen' ? <Image style={styles.defiDifficulteEtoiles} source={require('../../assets/img/difficulte_moyen.png')}/>
          : item.difficulty == 'Difficile' ? <Image style={styles.defiDifficulteEtoiles} source={require('../../assets/img/difficulte_difficile.png')}/>
          : item.difficulty == 'Extrême' ? <Image style={styles.defiDifficulteEtoiles} source={require('../../assets/img/difficulte_extreme.png')}/>
          : null}
        </View>
        <Text style={[styles.defiDifficulteXP, styles.textStyle]}>Points{"\n"}{item.xp}</Text>
      </View>
    </TouchableOpacity>
  </View>
);

const Defis = ({ navigation }) => {
  const [isLoading, setLoading] = useState(true);
  let [data, setData] = useState([]);

  useEffect(() => {
    fetch('http://whaledone.iut-lepuy.fr/api/challenges', {method: 'GET',
      headers: {
          'Content-Type'    : 'application/json',
          'Accept'          : 'application/json',
      }})
      .then((response) => response.json())
      .then((json) => setData(json))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  //console.log(data);

  const renderItem = ({ item }) => {
    return (
      <Item
        item={item}
        onPress={() => navigation.navigate("DetailDefi", {
          id: item.id,             
        })}
      />
    );
  };

  const renderHeader = () => {
    return (
      <View style={[styles.paddingHorizontal, styles.container__fullWidth]}>
        <View style={styles.center}>
          <Image
            style={[styles.baleineHeader, styles.mb0]}
            source={require('../../assets/img/logo_baleine_header.png')}
          />
        </View>
        <Text style={styles.introTitle}>Bravo !</Text>
        <Text style={styles.introText}>Tu as réalisé <Text style={[styles.introText, styles.fwBold]}>3</Text> défis cette semaine !</Text>
        <Image
          style={[styles.baleineBackground, {top: 80}]}
          source={require('../../assets/img/baleine_background.png')}
        />
      </View>
    );
  }

  const renderFooter = () => {
    return (
      <View style={[styles.center, {paddingBottom: 40, paddingTop: 20}]}>
        <TouchableOpacity style={styles.btnStyle}>
          <Text style={styles.btnTextStyle}>Accepter de nouveaux défis !</Text>
        </TouchableOpacity>
      </View>
    );
  }

  return (
    <View>
      {isLoading ? <ActivityIndicator size="large" color="#92A8D1" style={styles.activityIndicator}/> :
        <FlatList
          data={data}
          renderItem={renderItem}
          ListHeaderComponent={renderHeader}
          ListFooterComponent={renderFooter}
        />
      }
    </View>
  );
};

export default Defis;
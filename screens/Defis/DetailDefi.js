import React, { useEffect, useState } from "react";
import { View, Text, Image, TouchableOpacity, ActivityIndicator } from "react-native";
import styles from '../../style/style.js';

const DetailDefi = ({ route, navigation }) => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const params = route.params;
  let request = 'http://whaledone.iut-lepuy.fr/api/challenges/' + params.id;

  useEffect(() => {
    fetch(request)
    .then((response) => response.json())
    .then((json) => setData(json))
    .catch((error) => console.error(error))
    .finally(() => setLoading(false));
  }, []);

  //console.log(data);

  return (
    <View>
      {isLoading ? <ActivityIndicator size="large" color="#92A8D1" style={styles.activityIndicator}/> :
        <View style={styles.container}>
          <View style={[styles.flexRow, styles.spaceBetween, styles.alignItemsCenter]}>
            <Text style={[styles.defiTitle, styles.titleStyle]}>{data.title}</Text>
            {/* <Image
              style={styles.defiIcone}
              source={require('../../assets/img/pdp.jpg')}
            /> */}
          </View>
          <Text style={[styles.defiDescription, styles.textStyle]}>{data.summary}</Text>
          <View style={[styles.flexRow, styles.spaceBetween, styles.mb20]}>
            <View>
              <Text style={[styles.defiDifficulteXP, styles.textStyle]}>Difficulté</Text>
              {data.difficulty == 'Facile' ? <Image style={styles.defiDifficulteEtoiles} source={require('../../assets/img/difficulte_facile.png')}/>
              : data.difficulty == 'Moyen' ? <Image style={styles.defiDifficulteEtoiles} source={require('../../assets/img/difficulte_moyen.png')}/>
              : data.difficulty == 'Difficile' ? <Image style={styles.defiDifficulteEtoiles} source={require('../../assets/img/difficulte_difficile.png')}/>
              : data.difficulty == 'Extrême' ? <Image style={styles.defiDifficulteEtoiles} source={require('../../assets/img/difficulte_extreme.png')}/>
              : null}
            </View>
            <Text style={[styles.defiDifficulteXP, styles.textStyle]}>Points{"\n"}{data.xp}</Text>
          </View>
            
          <View style={styles.alignItemsCenter}>
            <TouchableOpacity style={[styles.btnStyle, styles.mb10, styles.btnStyleGreen]}>
              <Text style={[styles.btnTextStyle, styles.btnTextStyleGreen]}>Défi terminé</Text>
            </TouchableOpacity>
            
            <TouchableOpacity style={[styles.mb10]}>
              <Text style={styles.linkStyle}>Je le ferai plus tard</Text>
            </TouchableOpacity>
          </View>
        </View>
      }
    </View>
  );
};

export default DetailDefi;
import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import BottomTabNavigator from "./navigation/TabNavigator";
import * as Font from 'expo-font';

import LoginView from "./screens/Connexion/LoginView";

let customFont = {
  'Liquido': require('./assets/fonts/Liquido.otf'),
};

export default class App extends React.Component {

  state = {
    fontsLoaded: false,
  };

  async _loadFontsAsync() {
    await Font.loadAsync(customFont);
    this.setState({ fontsLoaded: true });
  }

  componentDidMount() {
    this._loadFontsAsync();
  }

  render() {
    if (this.state.fontsLoaded) {
      return (
        <NavigationContainer>
          {/* <BottomTabNavigator /> */}
          <LoginView />
        </NavigationContainer>
      );
    } else {
      return null;
    }
  }
}
